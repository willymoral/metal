//
//  ViewController.swift
//  MetalTraining
//
//  Created by Guillerm Moral on 03/04/2022.


import MetalKit

class ViewController: NSViewController {

    var renderer : RendererEngine = Renderer()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard let metalView = view as? MTKView else {
            fatalError("metal view not set up in storyboard")
        }
        
        self.renderer.initiliaze(metalView: metalView)
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
}

