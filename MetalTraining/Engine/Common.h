//
//  Common.h
//  MetalTraining
//
//  Created by GM on 03/04/2022.
//

#import <simd/simd.h>

typedef struct {
    matrix_float4x4 modelMatrix;
    matrix_float4x4 viewMatrix;
    matrix_float4x4 projectionMatrix;
} Uniforms;
