//
//  Primitive.swift
//  MetalTraining
//
//  Created by GM on 03/04/2022.
//

import MetalKit

class Primitive {
    class func cube(device: MTLDevice, size: Float) -> MDLMesh {
        let allocator = MTKMeshBufferAllocator(device: device)
        let mesh = MDLMesh(boxWithExtent: [size, size, size],
                       segments: [1, 1, 1],
                       inwardNormals: false, geometryType: .triangles,
                       allocator: allocator)
        return mesh
  }
}
