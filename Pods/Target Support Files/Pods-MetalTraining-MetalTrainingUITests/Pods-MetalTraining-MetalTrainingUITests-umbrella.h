#ifdef __OBJC__
#import <Cocoa/Cocoa.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif


FOUNDATION_EXPORT double Pods_MetalTraining_MetalTrainingUITestsVersionNumber;
FOUNDATION_EXPORT const unsigned char Pods_MetalTraining_MetalTrainingUITestsVersionString[];

